﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.DrawingCore;

namespace VisualChallengeGenerator
{
    public class PictureGen
    {
        static Random r = new Random((int)DateTime.Now.Ticks);
        public static Bitmap GetBitmap(string code)
        {
            Bitmap bitmap = new Bitmap(code.Length * 48 + 24, 128);
            Graphics g = Graphics.FromImage(bitmap);
            int xoff = 0;
            g.FillRegion(Brushes.White, new Region(new RectangleF(0, 0, code.Length * 48 + 24, 128)));
            foreach (char c in code)
            {
                g.DrawImageUnscaled(GetBitmapForChar(c), new Point(xoff - 24, r.Next(0, 14)));
                xoff += 48;
            }

            for (int i = 0; i < 30; i++)
            {
                g.DrawLine(new Pen(Brushes.Gray, (float)r.NextDouble() * 2f), new Point(r.Next(0, bitmap.Width * 2), r.Next(0, bitmap.Height * 2)), new Point(r.Next(0, bitmap.Height), r.Next(0, bitmap.Width)));
            }

            g.Save();

            for (int x = 0; x < bitmap.Width; x++)
                for (int y = 0; y < bitmap.Height; y++)
                {
                    Color color = bitmap.GetPixel(x, y);
                    color = Color.FromArgb((byte)Math.Min(color.A * add(5), 255),
                        (byte)Math.Min(color.R * add(70), 255),
                        (byte)Math.Min(color.G * add(70), 255),
                        (byte)Math.Min(color.B * add(70), 255));
                    bitmap.SetPixel(x, y, color);
                }

            return bitmap;
        }

        public static float add(int percent)
        {
            return 1f + ((float)r.NextDouble() - 0.5f) * percent / 100 * 2;
        }

        public static Bitmap GetBitmapForChar(char c)
        {
            Bitmap bitmap = new Bitmap(128, 128);
            Graphics g = Graphics.FromImage(bitmap);


            float size = 22;
            Font[] fonts = FontFamily.Families.Where(f => f.Name.ToLower().Contains("arial") ||
             f.Name.ToLower().Contains("comic") ||
              f.Name.ToLower().Contains("times") ||
              f.Name.ToLower().Contains("sans")).Select(f => new Font(f, size)).ToArray();
            Brush brush = new SolidBrush(Color.FromArgb(r.Next(0, 100),
                                                    r.Next(0, 100),
                                                    r.Next(0, 100)));
            Font font = fonts.ElementAt(r.Next(fonts.Count()));
            float rotate = r.Next(-30, 30);
            float scalex = 1f + r.Next(0, 40) / 100f;
            float scaley = 1f + r.Next(0, 40) / 100f;
            g.TranslateTransform(64, 64);
            g.RotateTransform(rotate);
            g.ScaleTransform(scalex, scaley);
            g.TranslateTransform(-64, -64);
            StringFormat drawFormat = new StringFormat();
            drawFormat.Alignment = StringAlignment.Center;
            drawFormat.LineAlignment = StringAlignment.Center;
            g.DrawString(c.ToString(), font, brush, new RectangleF(32, 23, 64, 64), drawFormat);
            g.TranslateTransform(64, 64);
            g.ScaleTransform(1 / scalex, 1 / scaley);
            g.RotateTransform(-rotate);
            g.ResetTransform();

            g.Save();

            return bitmap;
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHJKMNPQRSTLUVWXYZabcdeghkmnpqrsuvwxyz2345689";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[r.Next(s.Length)]).ToArray());
        }

        public static Bitmap GetRandomChallenge(int length)
        {
            return GetBitmap(RandomString(length));
        }
    }
}
